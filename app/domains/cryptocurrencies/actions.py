import requests

from settings import COINDESK_BPI_API


def get_bitcoin_price():
    return requests.get(COINDESK_BPI_API).json()


def get_value_bitcoin_price(value: float) -> float:
    bitcoin_data = get_bitcoin_price()
    bitcoin_value = bitcoin_data['bpi']['BRL']['rate_float']
    return (value * 1) / bitcoin_value