from typing import Dict, List

from app.domains.products.models import Product
from database.repository import save


def get_all() -> List[Product]:
    return Product.query.all()


def get_by_id(_id: str) -> Product:
    return Product.query.get(_id)


def create(data: Dict) -> Product:
    product = Product(**data)
    return save(product)


def update(_id: str, data: Dict) -> Product:
    product = get_by_id(_id)

    product.name = data.get('name')
    product.price = data.get('price')

    return save(product)
