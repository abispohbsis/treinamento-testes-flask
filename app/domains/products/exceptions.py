from typing import List

from app.exceptions import BadRequestException


class ProductCreateError(BadRequestException):
    def __init__(self, fields: List):
        msg = f"The following fields cannot be empty or None: {fields}"
        super().__init__(msg)
