from app.domains.products.actions import get_all as get_all_products, create as create_product

from flask import Blueprint, request, jsonify

app_products = Blueprint('app.products', __name__)


@app_products.route('/products', methods=['GET'])
def get():
    return jsonify([product.serialize() for product in get_all_products()]), 200


@app_products.route('/products', methods=['POST'])
def post():
    data = request.get_json()
    product = create_product(data)
    return jsonify(product.serialize()), 201
