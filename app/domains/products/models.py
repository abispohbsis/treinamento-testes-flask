from typing import Dict, NoReturn
import uuid

from app.domains.products.exceptions import ProductCreateError
from app.domains.cryptocurrencies.actions import get_value_bitcoin_price
from database import db


class Product(db.Model):
    id = db.Column(db.String(36), default=str(uuid.uuid4()), primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    price = db.Column(db.Float(), nullable=False)
    is_active = db.Column(db.Boolean(), nullable=False, default=True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._validate(**kwargs)

    def serialize(self) -> Dict:
        return {
            'id': self.id, 'name': self.name,
            'price': self.price, 'price_in_bitcoin': "{:.8f}".format(get_value_bitcoin_price(self.price))
        }

    def _validate(self, **data: Dict) -> NoReturn:
        invalid_fields = [key for key, value in data.items() if not value]

        if any(invalid_fields):
            raise ProductCreateError(invalid_fields)
