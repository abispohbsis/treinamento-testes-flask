[![pipeline](https://gitlab.com/abispohbsis/treinamento-testes-flask/badges/master/pipeline.svg)](https://gitlab.com/abispohbsis/treinamento-testes-flask/-/pipelines)
[![coverage report](https://gitlab.com/abispohbsis/treinamento-testes-flask/badges/master/coverage.svg)](https://abispohbsis.gitlab.io/treinamento-testes-flask)

# API Documentation

### Requirements
 
- Docker
- Docker compose

### Install

To install the dependencies, follow the command below:

`pipenv install`

### Environments
Please set `FLASK_ENV` into your operation system or favorite IDE

See below for values to `FLASK_ENV`:
* testing
* development

Next you need to configure the environment file `.env.development` to start development 
